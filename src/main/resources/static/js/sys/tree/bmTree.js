
$(function(){
    getTreeData();
});

//初始化树去后台拉去数据
function getTreeData() {
    $.ajax({
        type : "GET",
        url : "/bm/bmtree",
        success : function(tree) {
            loadTree(tree);
        }
    });
}

//初始化树
function loadTree(tree) {
    $('#jstree').jstree({
        'core' : {
            'data' : tree
        },
        "plugins" : [ "search" ]
    });
    $('#jstree').jstree();
}

//选择单位回调
$('#jstree').on("changed.jstree", function(e, data) {
    if(typeof (data.node)!="undefined") {
        if (data.node.parent == -1 ||data.node.parent == "#") {
            if (data.node.state.opened == true) {
                $('#jstree').jstree().close_node(data.selected);
            } else {
                $('#jstree').jstree().open_node(data.selected);
            }
        } else {
            var idxb = parent.idxb1;
            parent.$("#nrdwid"+idxb).val(data.selected[0]);
            parent.$("#nrdwnameid"+idxb).val(data.node.text);
            parent.zwdwChange(parent.$("#nrdwid"+idxb).val(),idxb);
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            // parent.parent.parent.parent.global.nrDwId = data.selected[0];
            // parent.parent.parent.parent.global.nrDwName = data.node.text;
        }
    }

});

function sear() {
    var name = $("#name").val();
    $.ajax({
        type: "GET",
        url: "/bm/bmtree?name=" + name,
        success: function (tree) {
            // loadTree(tree);
            $('#jstree').jstree(true).settings.core.data = tree; // 新数据
            $('#jstree').jstree(true).refresh(); //刷新树
        }
    });
}