package com.ifast.job.jobs;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.ifast.common.utils.DateUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * </pre>
 * 
 * <small> 2018年3月22日 | Aron</small>
 */
@Component
@Slf4j
public class ExamSmartJob implements Job {
	
//	@Autowired
//	private ExamSmartTimeService examSmartTimeService;
	
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("每周一题和每日一题定时任务| " + DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN_19)
                + " | 执行方法：\" + ExamSmartTimeService.createAnyTimeExam");
//        examSmartTimeService.createAnyTimeExam();
    }

}
