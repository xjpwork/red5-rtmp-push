package com.ifast.face.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ifast.face.dao.UserFaceImgDao;
import com.ifast.face.domain.UserFaceImg;
import com.ifast.face.service.UserFaceImgService;


@Service
public class UserFaceImgServiceImpl extends ServiceImpl<UserFaceImgDao, UserFaceImg> implements UserFaceImgService {}
