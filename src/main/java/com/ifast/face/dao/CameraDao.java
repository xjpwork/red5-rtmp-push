package com.ifast.face.dao;

import org.springframework.transaction.annotation.Transactional;

import com.ifast.common.base.BaseDao;
import com.ifast.face.domain.Camera;

@Transactional
public interface CameraDao extends BaseDao<Camera>{}
