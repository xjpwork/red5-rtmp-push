package com.ifast.face.controller;

import java.time.Duration;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ifast.websocket.OldWebSocketServer;

import reactor.core.publisher.Flux;

/** SSE 服务器发送事件
 */
@Controller
@RequestMapping("/hessian")
public class SSEController { 
	 
    @GetMapping(value="/push",produces="text/event-stream;charset=utf-8")
    @ResponseBody
    public String push() {
        Random r = new Random();
        
        Flux.range(1, 100)
		.delayElements(Duration.ofMillis(100))
		//.delaySubscription(Duration.ofMillis(100))
		//.take(Duration.ofNanos(984480l))
		//.subscribeOn(Schedulers.immediate())
		.subscribe(System.out::println);
        
       return "retry:60000\ndata:Testing 1,2,3" + r.nextInt() +"\n\n";
        
    }
    @GetMapping(value = "/sendTest")
    public String getSSEView () {
        return "face/send_test";
    }
    @GetMapping(value="/pushVideoListToWeb")
    @ResponseBody
    public String pushVideoListToWeb(String id) {
		  
		 try {
			 OldWebSocketServer.sendInfo("有新客户呼入,sltAccountId:"+id,id);
			 
		 }catch (Exception e) {
			  
		 }
		 return "yes";
	 }
 
}