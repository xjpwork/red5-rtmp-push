//package com.ifast.face.controller;
//
//import java.awt.BasicStroke;
//import java.awt.Color;
//import java.awt.Graphics2D;
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayOutputStream;
//import java.util.List;
//
//import javax.imageio.ImageIO;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.bytedeco.javacpp.opencv_core.Mat;
//import org.bytedeco.javacpp.opencv_videoio.VideoCapture;
//import org.bytedeco.javacv.FFmpegFrameGrabber;
//import org.bytedeco.javacv.Frame;
//import org.bytedeco.javacv.Java2DFrameConverter;
//import org.bytedeco.javacv.OpenCVFrameConverter;
//import org.bytedeco.javacv.OpenCVFrameConverter.ToMat;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.boot.autoconfigure.web.ServerProperties;
//import org.springframework.stereotype.Controller;
//import org.springframework.util.Base64Utils;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.arcsoft.face.FaceInfo;
//import com.ifast.Application;
//import com.ifast.common.base.AdminBaseController;
//import com.ifast.common.config.IFastConfig;
//import com.ifast.common.utils.SpringContextHolder;
//import com.ifast.face.dto.FaceSearchResDto;
//import com.ifast.face.dto.FaceUserInfo;
//import com.ifast.face.dto.ProcessInfo;
//import com.ifast.face.enums.ErrorCodeEnum;
//import com.ifast.face.service.FaceEngineService;
//import com.ifast.face.service.UserFaceInfoService;
//import com.ifast.face.util.ImageInfo;
//import com.ifast.face.util.ImageUtil;
//import com.ifast.face.util.Result;
//import com.ifast.face.util.Results;
//
//import cn.hutool.core.bean.BeanUtil;
//import cn.hutool.core.collection.CollectionUtil;
//
//@RequestMapping("/face/video")
//@CrossOrigin(origins = "*", maxAge = 3600)
//@Controller
//public class StartDemo2  extends AdminBaseController{ 
//	 
//	private String preHtml = "face/";
//	    
//    @Autowired
//    FaceEngineService faceEngineService;
//
//    @Autowired
//    UserFaceInfoService userFaceInfoService;
//    
//    
//    @RequestMapping(value = "/info")
//    public String demo() {
//        return preHtml + "video";
//    }
//    
//	public static void main(String[] args) throws Exception, InterruptedException {
//		//SpringApplication.run (StartDemo.class, args);
//		 
//		//2.启动摄像头 
//		 
//		String url2 = "http://admin:admin@192.168.0.49:8181";
//		
//		FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(url2);
//		System.out.println("-------1-----");
//		grabber.start(); // 开始获取摄像头数据 
//		while (true) { 
//			System.out.println("------2------");
////			Mat mat = new Mat();
////			 
////			ToMat converter = new OpenCVFrameConverter.ToMat();
////			  
//			 
//			ShowCamera.show(grabber.grabFrame()); 
//			 
//		} 
//		 
//	}
//	private static void printProjectConfigs() { 
//		ServerProperties serverProperties = SpringContextHolder.getApplicationContext().getBean(ServerProperties.class);
//		DataSourceProperties dataSourceProperties = SpringContextHolder.getApplicationContext().getBean(DataSourceProperties.class);
//		IFastConfig config = SpringContextHolder.getApplicationContext().getBean(IFastConfig.class);
//	}
//	
//	@RequestMapping(value = "/faceSearch", method = RequestMethod.GET) 
//    public void faceSearch(HttpServletRequest request,  HttpServletResponse httpServletResponse) throws Exception {
//
//		 
//    }
//	
//	public Result<FaceSearchResDto> faceOut(BufferedImage bufImage,Long groupId) throws Exception{
//		 
//        ImageInfo imageInfo = ImageUtil.bufferedImage2ImageInfo(bufImage);
//         
//        //人脸特征获取
//        byte[] bytes = null;//faceEngineService.extractFaceFeature(imageInfo); 
//
//        if (bytes == null) {
//            return Results.newFailedResult(ErrorCodeEnum.NO_FACE_DETECTED);
//        }
//        //人脸比对，获取比对结果
//        List<FaceUserInfo> userFaceInfoList = null;//faceEngineService.compareFaceFeature(bytes, groupId);
//
//
//        if (CollectionUtil.isNotEmpty(userFaceInfoList)) {
//            FaceUserInfo faceUserInfo = userFaceInfoList.get(0);
//            FaceSearchResDto faceSearchResDto = new FaceSearchResDto();
//            BeanUtil.copyProperties(faceUserInfo, faceSearchResDto);
//            List<ProcessInfo> processInfoList = faceEngineService.process(imageInfo);
//            if (CollectionUtil.isNotEmpty(processInfoList)) {
//                //人脸检测
//                List<FaceInfo> faceInfoList = faceEngineService.detectFaces(imageInfo);
//                int left = faceInfoList.get(0).getRect().getLeft();
//                int top = faceInfoList.get(0).getRect().getTop();
//                int width = faceInfoList.get(0).getRect().getRight() - left;
//                int height = faceInfoList.get(0).getRect().getBottom()- top;
//
//                Graphics2D graphics2D = bufImage.createGraphics();
//                graphics2D.setColor(Color.RED);//红色
//                BasicStroke stroke = new BasicStroke(5f);
//                graphics2D.setStroke(stroke);
//                graphics2D.drawRect(left, top, width, height);
//                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//                ImageIO.write(bufImage, "jpg", outputStream);
//                byte[] bytes1 = outputStream.toByteArray();
//                faceSearchResDto.setImage("data:image/jpeg;base64," + Base64Utils.encodeToString(bytes1));
//                faceSearchResDto.setAge(processInfoList.get(0).getAge());
//                faceSearchResDto.setGender(processInfoList.get(0).getGender().equals(1) ? "女" : "男");
//
//            } 
//            return Results.newSuccessResult(faceSearchResDto);
//        } 
//	    return Results.newFailedResult(ErrorCodeEnum.FACE_DOES_NOT_MATCH);
//	}
//} 