package com.ifast.face.dto;

import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author: ShiQiang
 * @Date: 2019年5月24日14:24:13
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ProcessInfo {
    private Integer age;
    private Integer gender;
    private FaceInfo face;
    private FaceFeature faceFeature;
    
}
