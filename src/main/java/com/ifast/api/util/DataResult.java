package com.ifast.api.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

public class DataResult<T> extends Result<T> {

	/**
	 * 单数据
	 */
	private Map<String,Object> data = Maps.newHashMap();
	
	/**
	 * 数组
	 */
	private List<Map<String,Object>> listData = Lists.newArrayList(); 
	 

	public DataResult() {
	    this.code = CODE_SUCCESS;
	    this.msg = MSG_SUCCESS;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public DataResult<?> setData(Map<String, Object> data) {
		this.data = data;
		return this;
	}

	public DataResult<?> put(String key , Object value){
		this.getData().put(key, value);
		return this;
	}

	public DataResult<?> put(Map<String, Object> map){
		this.getData().putAll(map);
		return this;
	}
	
	public List<Map<String, Object>> getListData() {
		return listData;
	}

	public DataResult<?> setListData(List<Map<String, Object>> listData) {
		this.listData = listData;
		return this;
	} 
	
	public DataResult<?> add(Map<String,Object> map){
		this.getListData().add(map);
		return this;
	}
	
	public static DataResult<?> okList(Map<String,Object> map) { 
        return new DataResult<>(CODE_SUCCESS, MSG_SUCCESS).add(map);
    }
	
	public static DataResult<?> okList(List<Map<String, Object>> listData) { 
        return new DataResult<>(CODE_SUCCESS, MSG_SUCCESS).setListData(listData);
    }
	
	public static DataResult<?> okData(Map<String,Object> map) { 
        return new DataResult<>(CODE_SUCCESS, MSG_SUCCESS).setData(map);
    }
	
	public static <T> DataResult<T> ok() {
        return new DataResult<T>(CODE_SUCCESS, MSG_SUCCESS);
    }

    public static <T> DataResult<T> fail() {
        return new DataResult<T>(CODE_FAIL, MSG_FAIL);
    }
    
    public static <T> DataResult<T> fail(String msg) {
        return new DataResult<T>(CODE_FAIL, msg);
    }
    
    public static <T> DataResult<T> fail(Integer code,String msg) {
        return new DataResult<T>(code, msg);
    }
    
    public DataResult(Integer code,String msg) {
         this.code = code;
         this.msg = msg;
    }
}
