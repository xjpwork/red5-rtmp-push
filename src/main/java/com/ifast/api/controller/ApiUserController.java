package com.ifast.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.ifast.api.util.DataResult;
import com.ifast.common.service.RedisParentManager;
import com.ifast.common.type.EnumErrorCode;
import com.ifast.common.utils.GjpLogger;
import com.ifast.sys.domain.UserDO;
import com.ifast.sys.service.UserService;

import cn.hutool.core.util.NumberUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <pre>
 *  基于jwt实现的API测试类
 * </pre>
 * 
 * <small> 2018年4月27日 | Aron</small>
 */
@RestController
@RequestMapping("/api/user/")
public class ApiUserController {

    @Autowired
    private UserService sysUserServiceImpl;

    @Autowired
    private RedisParentManager redisParentService;


    @GetMapping("getUserInfo")
    @ApiOperation("获取个人信息")
    @RequiresAuthentication
    public DataResult<?> getUserInfo(@RequestHeader String Authorization) {
        return this.sysUserServiceImpl.getUserInfoForApp(Authorization);
    }

    @PostMapping("resetPassword")
    @ApiOperation("修改密码")
    @RequiresAuthentication
    public DataResult<?> resetPassword(
            @ApiParam(name = "oldPassword", required = true, value = "旧密码") @RequestParam("oldPassword") String oldPassword,
            @ApiParam(name = "newPassword", required = true, value = "新密码") @RequestParam("newPassword") String newPassword,
            @ApiParam(name = "Authorization", required = true, value = "token") @RequestHeader("Authorization") String token
    ) {
        return this.sysUserServiceImpl.updatePassword(token,oldPassword,newPassword);
    }


    @PostMapping("updateUserPic")
    @ApiOperation("更新个人头像")
    @RequiresAuthentication
    public DataResult<?> updateUserPic(@RequestHeader String Authorization, HttpServletRequest request) {
        try {
            return this.sysUserServiceImpl.updateUserInfoZjForApp(Authorization,request);
        } catch (Exception e) { 
            GjpLogger.error(e.toString(),e);
            return DataResult.fail(EnumErrorCode.apiYwClFailed.getCode(), EnumErrorCode.apiYwClFailed.getMsg());
        } 
    }


    @PostMapping("forgetPassword")
    @ApiOperation("忘记密码")
    public DataResult<?> forgetPassword(
            @ApiParam(name = "userName", required = true, value = "账号") @RequestParam("userName") String userName,
            @ApiParam(name = "code", required = true, value = "验证码") @RequestParam("code") String code,
            @ApiParam(name = "newPassWord", required = true, value = "新密码") @RequestParam("newPassWord") String newPassWord
    ) {
        return this.sysUserServiceImpl.forgetPassword(code,userName,newPassWord);
    }



    @GetMapping("getSmsCode")
    @ApiOperation("获取验证码")
    public DataResult<?> getSmsCode(
            @ApiParam(name = "tel", required = true, value = "手机号码") @RequestParam("tel") String tel) {
        DataResult<?> dataResult = new DataResult<>();
        //判断用户是否存
        UserDO user = new UserDO();
        user.setUsername(tel);
        user = sysUserServiceImpl.getOne(new QueryWrapper<>(user));
        if(user == null || user.getId() == null){
        	 dataResult.setCode(EnumErrorCode.apiYwClFailed.getCode());
             dataResult.setMsg("用户不存在!");
             return dataResult;
        }
        //判断规定时间内是否已发送
        if(redisParentService.containsValueKey(tel)){
            dataResult.setCode(EnumErrorCode.apiYwClFailed.getCode());
            dataResult.setMsg("验证码已发送!");
            return dataResult;
        }
        List<String> params = Lists.newArrayList();
        int[] sjz = NumberUtil.generateRandomNumber(0,9,4);
        StringBuilder codez = new StringBuilder();
        for (int z : sjz) {
            codez.append(z);
        }
        //redisParentService.cacheValue(tel,codez.toString(),""); 
        params.add(codez.toString());
//        SmsSingleSenderResult smsSingleSenderResult = SmsUtil.singleSend(tel,params,
//                316184);
        int fee = 0;//smsSingleSenderResult.fee;
        if(fee != 0){
            dataResult.put("code",codez.toString());
        }else{
            dataResult.setCode(EnumErrorCode.apiYwClFailed.getCode());
            dataResult.setMsg(EnumErrorCode.apiYwClFailed.getMsg());
        }
        return dataResult;

    }
}
