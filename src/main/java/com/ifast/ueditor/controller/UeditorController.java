package com.ifast.ueditor.controller;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baidu.ueditor.ActionEnter;
import com.ifast.common.base.AdminBaseController;

/**
 * @ClassName UeditorController
 * @Author hailong.zheng
 * @Date 2018/9/19 10:00
 * @Version v1.0
 */
@Controller
public class UeditorController extends AdminBaseController {
    /**
     * 百度富文本编辑器
     * @param request
     * @param response
     */
    @RequestMapping(value="/ueditor/config")
    public void config(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/json");
        String rootPath = request.getSession().getServletContext().getRealPath("/");
        try(PrintWriter writer = response.getWriter();){
            String exec = new ActionEnter(request, rootPath).exec(); 
            writer.write(exec);
            writer.flush(); 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
