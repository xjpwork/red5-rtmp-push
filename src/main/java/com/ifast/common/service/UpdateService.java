package com.ifast.common.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.ifast.api.util.DataResult;
import com.ifast.common.base.CoreService;
import com.ifast.common.domain.Update;

/**
 * @ClassName UpdateService
 * @Author ShiQiang
 * @Date 2019年4月24日14:14:23
 * @Version v1.0
 */
@Service
public interface UpdateService extends CoreService<Update> {

	Update getUpdate(String type);
    /**新增*/
	public DataResult<?> add(HttpServletRequest request,Update update)throws IOException;
	
}
