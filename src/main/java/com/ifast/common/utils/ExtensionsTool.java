package com.ifast.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class ExtensionsTool {

	public static String format(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(date);
	}

	public static <K, V> Map<K, V> newHashMap(Object hashMap, K key, V value) {
		Map<K, V> map = new HashMap<>();
		map.put(key, value);
		return map;
	}

	public static <K, V> boolean isNull(Map<K, V> map) {
		return map == null || map.size() <= 0;
	}

	public static <K, V> boolean isNotNull(Map<K, V> map) {
		return !isNull(map);
	}

	public static boolean isNull(String str) {
		return StringUtils.isBlank(str);
	}

	public static boolean isNotNull(String str) {
		return StringUtils.isNotBlank(str);
	}

	public static <K, V> boolean isNull(List<K> list) {
		return list == null || list.size() <= 0;
	}
	/**
	 * 不是null 返回自身，是null返回其他 
	 */
	public static <T> T or(T obj, T ifNull) {
		return obj != null ? obj : ifNull;
	}
	
	public static <K, V> Map<K,V> put(Map<K, V> map,K key, V value){
		map.put(key, value);
		return map;
	}
	
}