package com.ifast.common.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;

public class WordExportPOI {
    //字体
    public final static Integer FONT_SIZE_14 =14;
    public final static Integer FONT_SIZE_18 =18;
    public final static Integer FONT_SIZE_16 = 16;

    public static void   wordFile(OutputStream out,XWPFDocument document){
        try {
            document.write(out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /** 列表*/
    public static XWPFDocument title(XWPFDocument document, String title, String color,Integer size){
        XWPFParagraph titleText = document.createParagraph();
        //设置段落居中
        titleText.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun titleParagraphRun = titleText.createRun();
        titleParagraphRun.setText(title);
        if(color!=null){
            titleParagraphRun.setColor(color);
        }
        if(size !=null){
            titleParagraphRun.setFontSize(size);
        }else{
            titleParagraphRun.setFontSize(20);
        }
        return document;
    }
    /**段落*/
    public static XWPFDocument paragraph(XWPFDocument document,String text,String color,Integer size,String backGroundColor,Boolean nextRow){
        XWPFParagraph firstParagraph = document.createParagraph();
        XWPFRun run = firstParagraph.createRun();
        run.setText(text);
        //设置字体颜色
        if(color !=null){
            run.setColor(color);
        }
        //设置字体大小
        if(size!=null){
            run.setFontSize(size);
        }else{
            run.setFontSize(16);
        }
        // 设置背景 （高亮）
        if(backGroundColor!=null){
            CTShd cTShd = run.getCTR().addNewRPr().addNewShd();
            cTShd.setVal(STShd.CLEAR);
            cTShd.setFill(backGroundColor);
        }
        //换行
        if(nextRow){
            XWPFParagraph paragraph1 = document.createParagraph();
            XWPFRun paragraphRun1 = paragraph1.createRun();
            paragraphRun1.setText("\r");
        }
        return document;
    }
    
    public static void pesponesword(HttpServletResponse response, String fileName, XWPFDocument document){
        try ( ByteArrayOutputStream ostream = new ByteArrayOutputStream();
                OutputStream out = response.getOutputStream();){
            //输出word内容文件流，提供下载
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment;filename=" + new String((fileName).getBytes(), "iso-8859-1"));
            document.write(ostream);
            out.write(ostream.toByteArray());
            out.flush();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void inputZipFile(HttpServletResponse response,String fileName,List<XWPFDocument> documentList){
        try(ZipOutputStream zos = new ZipOutputStream(response.getOutputStream());){ 
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment;filename="
                    + new String(fileName.getBytes("GB2312"), "8859_1")
                    + ".zip");
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
            for(XWPFDocument doc :documentList){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
                String dateTime = LocalDateTime.now(ZoneOffset.of("+8")).format(formatter);
                zos.putNextEntry(new ZipEntry(fileName+dateTime+".doc"));
                ByteArrayOutputStream ostream = new ByteArrayOutputStream();
                doc.write(ostream);
                ByteArrayInputStream swapStream = new ByteArrayInputStream(ostream.toByteArray());
                BufferedInputStream  bis = new BufferedInputStream(swapStream, 1024 * 10);
                byte[] bufs = new byte[1024 * 10];
                int read = 0;
                while ((read = bis.read(bufs, 0, 1024 * 10)) != -1) {
                    zos.write(bufs, 0, read);
                }
                zos.flush();
            } 
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
