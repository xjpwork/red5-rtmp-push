package com.ifast.common.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @ClassName Update
 * @Author ShiQiang
 * @Date 2019年4月24日10:27:19
 * @Version v1.0
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("TB_UPDATE")
public class Update implements Serializable {

    private static final long serialVersionUID = -5534338148165700471L;
    @TableId(type= IdType.UUID)
    private String id;
    private boolean forced; //forced True代表此版本需要进行强制更新
    private boolean ignore; // ignore True代表在弹出通知中进行展示 <b>忽略此版本更新按钮</b>
    private String updateContent; //设置此次版本更新内容，将在更新弹窗通知中使用
    private String updateUrl; // 设置此次版本的远程更新apk包
    private int versionCode;	// apk版本号
    private String versionName; // 新版本的apk的版本名。
    private String md5;	//指定下载文件的md5值。用于对下载文件进行检查时使用。
    private String type;
}
