package com.ifast.sys.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ifast.common.base.BaseDao;
import com.ifast.sys.domain.DeptDO;

/**
 * <pre>
 * 部门管理
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
public interface DeptDao extends BaseDao<DeptDO> {
	
	Long[] listParentDept();
	
	int getDeptUserNumber(String deptId);

	int listPage(String deptId);

	List<?> listPage(IPage<?> page, DeptDO deptDO);

	List<DeptDO> listDeptTree(DeptDO deptDO);

	List<DeptDO> listDeptUpTree(DeptDO deptDO);
}
