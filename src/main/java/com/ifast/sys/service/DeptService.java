package com.ifast.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ifast.common.base.CoreService;
import com.ifast.common.domain.Tree;
import com.ifast.sys.domain.DeptDO;

/**
 * <pre>
 * 部门管理
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
public interface DeptService extends CoreService<DeptDO> {
    
	Tree<DeptDO> getTree(String depId);
	
	boolean checkDeptHasUser(String deptId);

	IPage<DeptDO> listPage(Page<?> page, DeptDO deptDO);

	List<DeptDO> listDeptUpTree(DeptDO deptDO);
}
