package com.ifast.hessian.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beust.jcommander.internal.Lists;
import com.ifast.face.dao.UserFaceImgDao;
import com.ifast.face.domain.UserFaceImg;
import com.ifast.face.domain.UserFaceInfo;
import com.ifast.face.dto.FaceSearchResult;
import com.ifast.face.service.FaceEngineService;
import com.ifast.face.service.UserFaceInfoService;
import com.ifast.face.util.DaemonExcutor;
import com.ifast.hessian.domain.FaceSearchHassanResult;
import com.ifast.hessian.service.CheckFaceService;
import com.xiaoleilu.hutool.date.DateUtil;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
@Service
public class CheckFaceServiceImpl extends ServiceImpl<UserFaceImgDao, UserFaceImg> implements CheckFaceService{
	
	@Autowired
	private FaceEngineService faceEngineService;
	@Autowired
	private UserFaceInfoService userFaceInfoService;
	@Autowired
	private UserFaceImgDao userFaceImgDao;
	
	@Override
	public List<FaceSearchHassanResult> toCheck(Long groupId){
		List<FaceSearchHassanResult> result = Lists.newArrayList(); 
		List<UserFaceImg> data = new UserFaceImg().selectAll();
		System.out.println(data.size()+"-----------=======----------");
		if (CollectionUtil.isNotEmpty(data)){
			userFaceImgDao.deleteBatchIds(data.stream().map(img -> img.getId()).collect(Collectors.toList()));
			for (UserFaceImg img : data) {
				try (ByteArrayInputStream in = new ByteArrayInputStream(img.getImg());) {
					BufferedImage image = ImageIO.read(in);
					List<FaceSearchResult> usersInDb = faceEngineService.findUsersByFaces(image,groupId);
					
					if (CollectionUtil.isNotEmpty(usersInDb)) {
						for(FaceSearchResult source : usersInDb){
							FaceSearchHassanResult  target = new FaceSearchHassanResult();
							BeanUtil.copyProperties(source, target);
							target.setImg(img.getImg());
							result.add(target);
						}
						doUpdateUserInfo(usersInDb);
					}
					image.flush();
				} catch (Exception e) {
					
				} 
			}
		}
		System.out.println("-----------返回----------");
		return result;
	} 
	
	private void doUpdateUserInfo(List<FaceSearchResult> usersInDb){ 
		List<UserFaceInfo> users = (List<UserFaceInfo>) userFaceInfoService.listByIds(usersInDb.stream().map(search -> search.getId()).collect(Collectors.toList()));
		if(CollectionUtil.isEmpty(users)){
			return ;
		} 
		users.stream().forEach(user -> {
			user.setChecked(1);
			user.setUpdateTime(DateUtil.now());
			faceEngineService.refresh(user.getGroupId());
		});
		userFaceInfoService.updateBatchById(users); 
	}

	@Override
	public boolean addToCache(Long groupId,Long userFaceInfoId){
		try { 
			faceEngineService.refresh(groupId); 
			return true;	
		} catch (Exception e) {
			return false;
		} 
	}

	@Override
	public void changePassRate(int passRate) {
		DaemonExcutor.passRate = passRate;
	}
}
