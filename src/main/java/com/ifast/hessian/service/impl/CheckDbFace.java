package com.ifast.hessian.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.ifast.face.domain.UserFaceImg;
import com.ifast.face.domain.UserFaceInfo;
import com.ifast.face.dto.FaceSearchResult;
import com.ifast.face.service.FaceEngineService;
import com.ifast.face.service.UserFaceInfoService;
import com.xiaoleilu.hutool.date.DateUtil;

import cn.hutool.core.collection.CollectionUtil;

public class CheckDbFace implements Runnable { 
	 
	private FaceEngineService faceEngineService;
	private UserFaceInfoService userFaceInfoService;
	private String groupId;
	
	private List<UserFaceImg> data;
	public CheckDbFace(String groupId,List<UserFaceImg> data, 
			FaceEngineService faceEngineService, UserFaceInfoService userFaceInfoService) {
		this.data = data;
		this.groupId = groupId;
		this.faceEngineService = faceEngineService;
		this.userFaceInfoService = userFaceInfoService;
		 
	}
 
//	private List<UserFaceImg> getData() {
//		try {
//			if (CollectionUtil.isEmpty(data)) {
//				synchronized(CheckDbFace.class){
//					data = userFaceImgService.selectList(new EntityWrapper<UserFaceImg>().eq("state", "0"));
//					if (CollectionUtil.isEmpty(data)) {
//						synchronized (CheckDbFace.class) {
//							data = userFaceImgService.selectList(new EntityWrapper<UserFaceImg>().eq("state", "0"));
//						} 
//					}
//					if(CollectionUtil.isNotEmpty(data)){
//						data.stream().forEach(img -> img.setState(1));
//						userFaceImgService.updateBatchById(data);
//					} 
//				}
//			} 
//		} catch (Exception e) {
//			 
//		}
//		return data;
//	}

	@Override
	public void run() {
		try {
			doCheck();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void doCheck() {
		
		for (UserFaceImg img : data) {
			try (ByteArrayInputStream in = new ByteArrayInputStream(img.getImg());) {
				BufferedImage image = ImageIO.read(in);
				List<FaceSearchResult> usersInDb = faceEngineService.findUsersByFaces(image,Long.valueOf(groupId));

				if (CollectionUtil.isNotEmpty(usersInDb)) {
					// WebCamPreviewController.toCheck = false;
					doUpdateUserInfo(usersInDb);
					//showFace(image, usersInDb);
					// 弹出
					// checkSuccess(usersInDb, hasCheckedFace);
				}
				image.flush();
			} catch (Exception e) {
				// TODO: handle exception
			} 
		}
		System.out.println("检验-数据库-人脸-数据");
		data.clear();
	}

	private void doUpdateUserInfo(List<FaceSearchResult> usersInDb) {
		List<UserFaceInfo> users = (List<UserFaceInfo>) userFaceInfoService.listByIds(usersInDb.stream().map(search -> search.getId()).collect(Collectors.toList()));
		if (CollectionUtil.isEmpty(users)) {
			return;
		}
		users.stream().forEach(user -> {
			user.setChecked(1);
			user.setUpdateTime(DateUtil.now());
			faceEngineService.refresh(user.getGroupId());
		});
		userFaceInfoService.updateBatchById(users);
		 
	}

	
}